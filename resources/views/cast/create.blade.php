@extends('layout.master')

@section('judul')
    Halaman Tambah Cast
@endsection

@section('isi')
<div>
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Cast</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/cast" method="POST">
      @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" name="nama" class="form-control" id="nama" placeholder="Masukan Nama">
          @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
        </div>
        <div class="form-group">
          <label for="umur">Umur</label>
          <input type="number" name="umur" class="form-control" id="umur" placeholder="Masukan Umur">
          @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
        </div>
        <div class="form-group">
          <label for="bio">Bio</label>
          <textarea class="form-control" name="bio" id="" cols="30" rows="5"></textarea>
          @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
        </div>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
</div>

@endsection